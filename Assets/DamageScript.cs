﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageScript : MonoBehaviour, IDamage<float>
{
    [SerializeField] private float startingHealth = 100;
    public float health { get; set; }

    private void Start()
    {
        health = startingHealth;
    }

    public void ApplyDamage(float damageAmount)
    {
        health = health - damageAmount;
        if (health < 0) health = 0;
        if (health == 0) Debug.Log("Dead!");
    }
}
