﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClockPowerup : MonoBehaviour
{

    public CapsuleCollider2D mcCollider;
    private CapsuleCollider2D powerupCollider;
    
    // this is a dummy object with just a sprite for visual purposes
    public GameObject clockPowerup; 
    
    // Start is called before the first frame update
    void Start()
    {
        powerupCollider = gameObject.GetComponent<CapsuleCollider2D>();
    }
    
    // when mc collides with the powerup, the powerup will be destroyed and the obstacles will slow down 
    private void OnTriggerEnter2D(Collider2D other){
        if (other.name.Equals(mcCollider.name) && !other.isTrigger) {
            Destroy(clockPowerup);
            SlowDown();
        }
    }
    
    // this function slows down all the presses to half speed
    private void SlowDown(){
        var presses= GameObject.FindWithTag("press").GetComponentsInChildren<Transform>();
        foreach (var press in presses){
            if (press.gameObject.GetComponent<Animator>() != null){
                press.gameObject.GetComponent<Animator>().speed = .5f;
            }
        }
        StartCoroutine(Revert());
    }
    
    // after a 30 second delay, this changes the speed of the presses back to normal
    private IEnumerator Revert(){
        Debug.Log("inside revert");
        yield return new WaitForSeconds(30f);
        var presses= GameObject.FindWithTag("press").GetComponentsInChildren<Transform>(); 
        foreach (var press in presses){
            if (press.gameObject.GetComponent<Animator>() != null){
                press.gameObject.GetComponent<Animator>().speed = 1;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
