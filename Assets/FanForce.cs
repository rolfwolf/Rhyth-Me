﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FanForce : MonoBehaviour
{
    [SerializeField] BoxCollider2D fanZone;
    [SerializeField] int fanStrength = 35;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        CheckIfCharacterOverFan();
    }

    void CheckIfCharacterOverFan()
    {
        int resultCount = 5;
        Collider2D[] hitColliders = new Collider2D[5];
        ContactFilter2D contactFilter = new ContactFilter2D();
        resultCount = fanZone.OverlapCollider(contactFilter, hitColliders);
        Debug.Log("Fan overlaps: " + resultCount);
        foreach (Collider2D collider in hitColliders)
        {
            if (collider == null) { continue; }
            collider.gameObject.GetComponent<Rigidbody2D>().AddForce(transform.up * fanStrength);

        }
    }
}
