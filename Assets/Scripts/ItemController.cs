﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ItemController : MonoBehaviour
{
    public CapsuleCollider2D mcCollider;
    private CapsuleCollider2D coinCollider;
    // Start is called before the first frame update
    void Start()
    {
        coinCollider = gameObject.GetComponent<CapsuleCollider2D>();
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D other){
        if (other.name.Equals(mcCollider.name) && !other.isTrigger) {
            Destroy(gameObject);
           
        }
    }
}
