# Rhyth-Me

A 2D rhythm platformer game set in an urban environment. 

## Installation 

1. Navigate to the [Unity archives page](https://unity3d.com/get-unity/download/archive) and download version 2020.1.2. 
2. Clone this repo.
3. Open Unity Hub, click the Add button, and navigate to where the cloned repo's folder is. 
4. Add the folder, and then open it through the Unity Hub, making sure that the version is 2020.1.2.
5. Unity will then open the project. If there are Library/PackageCache errors, open up a file explorer, navigate to the Library folder within the cloned repo, and delete it. Open the project through Unity Hub again, and when prompted to downgrade to 2020.1.2, click yes.

## [Coding Documentation](https://docs.google.com/document/d/1JIpgr0jOWqHKCF01nD6vjH53L-Aj8iCN1w4_KL9CBcc/edit?usp=sharing)